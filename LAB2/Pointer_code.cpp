#include <stdio.h>

void update(int *a, int *b) {
	int sum = *a + *b;
	int absValue;
	if (*a - *b > 0) {
		absValue = *a - *b;
	}
	else {
		absValue = -(*a - *b);
	}
	*a = sum;
	*b = absValue;
}

int main() {
	int a, b;
	int *pa = &a, *pb = &b;

	scanf("%d %d", &a, &b);
	update(pa, pb);
	printf("%d\n%d", a, b);

	return 0;
}