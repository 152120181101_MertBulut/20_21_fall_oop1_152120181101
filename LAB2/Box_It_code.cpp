#include<bits/stdc++.h>
#include <iostream>
#include <ostream>
using namespace std;

class Box{
private:
	int l, b, h;

public:
	Box()
	{
		this->l = 0;
		this->b = 0;
		this->h = 0;
	}

	Box(int l, int b, int h)
	{
		this->l = l;
		this->b = b;
		this->h = h;
	}

	Box(const Box &B)
	{
		this->l = B.l;
		this->b = B.b;
		this->h = B.h;
	}

	int getLength()
	{
		return l;
	}

	int getBreadth()
	{
		return b;
	}

	int getHeight()
	{
		return h;
	}

	long long CalculateVolume()
	{
		long long vol;
		vol = l;
		vol *= b;
		vol *= h;
		return vol;
	}

	bool operator < (Box const &B)
	{
		if (l < B.l)
			return true;
		else if (b < B.b && l == B.l)
			return true;
		else if (h < B.h && b == B.b && l == B.l)
			return true;
		else
			return false;
	}

	friend ostream& operator << (ostream& os, const Box &B)
	{
		os << B.l << " " << B.b << " " << B.h;
		return os;
	}
};

