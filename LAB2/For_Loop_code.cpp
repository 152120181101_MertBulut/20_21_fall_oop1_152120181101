#include <iostream>
#include <string>
#include <cstdio>
using namespace std;

int main() {

	int s1, s2;
	cin >> s1 >> s2;

	string num[] = {"one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};

	for (int i = s1; i <= s2; i++) {
		if (i > 9) {
			if (i % 2 == 0)
				cout << "even" << endl;
			else
				cout << "odd" << endl;
		}
		else {
			cout << num[i - 1] << endl;
		}
			
	}
	return 0;
}