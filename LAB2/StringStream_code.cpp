#include <sstream>
#include <vector>
#include <iostream>
using namespace std;

vector<int> parseInts(string str) {

	vector<int> v1;
	int temp;
	char c;

	stringstream sts(str);

	while (sts >> temp) {

		v1.push_back(temp);
		sts >> c;
	}

	return v1;
};

int main() {
	string str;
	cin >> str;
	vector<int> integers = parseInts(str);
	for (int i = 0; i < integers.size(); i++) {
		cout << integers[i] << "\n";
	}

	return 0;
}