/**
 * @file main.cpp
 * @author MERT BULUT 152120181101<br>
 * @date 03.11.2020
 */
#include <iostream> 
#include<string> 
#include<fstream> 
#include <array>

using namespace std;

void ReadFile(int* &Arry, int &ArrySize, int &numCount);
void Sum(int* Arry, int ArrySize, int &sum);
void Product(int* Arry, int ArrySize);
void Average(int ArrySize, int sum);
void Smallest(int* Arry, int ArrySize);
/**
 * ArrySize variable is used to determine the size of the dynamic array.<br>
 * numCount is defined for comparison. Keeps the count nums.<br>
 * Arry is a pointer variable to create a dynamic array.
 * \code
 * if (numCount == ArrySize)
 * \endcode
 * \note It compares the number of numbers entered with the size of the given array. 
 * \param ArrySize an integer.
 * \param *Arry an integer.
 * \param numCount an integer.
 * \param sum an integer.
 */
int main() {
	int ArrySize;
	int *Arry = NULL;
	int numCount = 0;
	int sum = 0;
	ReadFile(Arry, ArrySize, numCount);
	if (numCount == ArrySize && ArrySize > 0) {
		Sum(Arry, ArrySize, sum);
		Product(Arry, ArrySize);
		Average(ArrySize, sum);
		Smallest(Arry, ArrySize);
	}
	else {
		cout << "Missing or incorrect data entry.." << endl;
		cout << "Please check your file.. " << endl;
	}
		
	system("pause");
	return 0;
}
/**
 * ReadFile is a function for reading file.<br>
 * In input.txt, it reads the size to reserve for the array in the first line.<br>
 * In input.txt, it assigns integer values ​​in the second line into the array.
 * \code
 * if (file >> Arry[numCount]) {}
 * if (file.peek() != fstream::traits_type::eof()) {}
 * \endcode
 * \note It is defined to control and prevent the entry of characters other than integers.
 * \note It checks whether the file is empty or full.
 * \param filename an string.
 */
void ReadFile(int* &Arry, int &ArrySize, int &numCount) {
	string filename = "input.txt";
	fstream file(filename, ios::in);
	if (file.is_open()) {
		if (file.peek() != fstream::traits_type::eof()) {
			file >> ArrySize;
			Arry = new int[ArrySize];

			while (!file.eof()) {
				if (file >> Arry[numCount]);
				else {
					cout << "The values in the input.txt file must be integers." << endl;
					break;
				}
				numCount++;
			}
		}
		else {
			cout << "File is empty.." << endl;
		}
			
	}
	else {
		cout << "input.txt file not found.. " << endl;
	}
}


/**
 * Sum function sums the numbers in the array and prints to the screen.
 * \param i an integer.
 */

void Sum(int* Arry, int ArrySize, int &sum) {

	for (int i = 0; i < ArrySize; i++) {
		sum += Arry[i];
	}
	cout << endl;
	cout << "Sum is " << sum << endl;

}

/**
 * Product function multiplications the numbers in the array and prints to the screen.
 * \param product an integer.
 * \param i an integer.
 */

void Product(int* Arry, int ArrySize) {

	int product = 1;
	for (int i = 0; i < ArrySize; i++) {
		product *= Arry[i];
	}
	cout << "Product is " << product << endl;
}

/**
 * Average function calculates the average by dividing the sum we got from the Sum function by the size of the array and prints to the screen.
 * \param average an float.
 */

void Average(int ArrySize, int sum) {

	float average = 0;
	average = float(sum) / ArrySize;
	cout << "Average is " << average << endl;
}

/**
 * Smallest function finds the smallest number in the array and prints to the screen.
 * \param smallest an integer.
 * \param i an integer.
 */

void Smallest(int* Arry, int ArrySize) {

	int smallest = 0;
	smallest = Arry[0];
	for (int i = 1; i < ArrySize; i++) {
		if (Arry[i] < smallest) {
			smallest = Arry[i];
		}
	}
	cout << "Smallest is " << smallest << endl;
	cout << endl;
}